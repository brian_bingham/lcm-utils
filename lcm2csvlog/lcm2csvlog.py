#!/usr/bin/env python
'''
Logging utility for LCM publication

Basic idea is to provide simple, general-purpose functionality for LCM publications to be written to a CSV file to simplify data management, i.e., avoid post-processing LCM logs.  

There are two modes of functionality: Real-time and Post-process

In Real-Time mode, the following is true:
 * The ouput CSV file name is created based on the current time and the user specified channel for reading and the user specified file label- e.g., "20170815_093554_CHANNEL_.csv"
 * The time used for both timestamping the incoming LCM data (written to CSV file in first column) and determining the decimation (see sample-time option) is the current system time, i.e., time.time()

In Post-Process mode, the folling is true:
 *  The output CSV file name is created based on the input LCM log file name and the channel and the optional file namel e.g., "lcm2csvlog --input_logfile=20170806_yogi_still.lcmlog --file_label=LABEL NAV_STAT" generates the CSV file named "20170806_yogi_still.lcmlog_NAV_STAT_LABEL_.csv"
 * The time used for both timestamping the incoming LCM data (written to CSV file in first column) and determining the decimation (see sample-time option) is the LCM logfile timestamp (i.e., event.timestamp)
'''

import socket
import sys
import math
import os
import datetime
import logging
import serial
import time
import argparse
import inspect

import lcm
# OER LCM message types
import gss
import oer
# OER Utilities
from oer_utils import get_sender_id, get_unix_time, argparse_utils, get_config_val

def find_lcm_type(msgdata):
    
    lcmpackages = [gss,oer]
    lcmtype = None
    for p in lcmpackages:
        types = inspect.getmembers(p,inspect.isclass)
        for t in types:
            try:
                msg = t[1].decode(msgdata)
                lcmtype = t[1]
                logging.info("Found LCM type <%s>"%lcmtype)
                break
            except ValueError:
                pass #logging.debug("LCM type <%s> doesn't work"%t[1])
    return lcmtype
                
def get_lcm_fields(msg):
    '''
    Return a list of data fields contained in a decoded LCM message
    '''
    full = dir(msg)
    # a little list comprehension to get rid of any attributes 
    # that start with an underscore or specific methods.
    fields = [x for x in full if not(x[0]=='_') 
              if not(x=='decode')
              if not(x=='encode')]
    return fields

##def val2list(name,val):
#    hlist, vlist = v2list(name,val)
    # Flatten
    
def val2list(name,val):
    '''
    Takes in the name of a field and the value of that field.
    If the value is a list it unpacks the list into indexed names and 
    corresponding values
    Using a little recursion
    '''
    hlist = []
    vlist = []
    if hasattr(val,'__iter__'):
        for v,ii in zip(val,range(len(val))):
            h,v = val2list(name+'[%d]'%ii,v)
            if isinstance(h,list):
                hlist+=h
                vlist+=v
            else:
                hlist.append(h)
                vlist.append(v)
    else:
        hlist = [name]
        vlist = [str(val)]
    return hlist,vlist

def pcomms2list(msg):
    names = ['unix_time','sender_id','num_analogs']
    vals = [str(msg.unix_time),msg.sender_id,str(msg.num_analogs)]
    for ii in range(msg.num_analogs):
        names.append('analogs[%d].name'%ii)
        vals.append(str(msg.analogs[ii].name))
        names.append('analogs[%d].value'%ii)
        vals.append(str(msg.analogs[ii].value))
    names.append('num_digitals')
    vals.append(str(msg.num_digitals))
    for ii in range(msg.num_digitals):
        names.append('digitals[%d].name'%ii)
        vals.append(str(msg.digitals[ii].name))
        names.append('digitals[%d].name'%ii)
        vals.append(str(msg.digitals[ii].value))
    return names, vals
    

def msg2list(msg,fields=None):
    head = []
    csv = []
    # Special treatment for pcomms
    if isinstance(msg,gss.pcomms_t):
        namelist, vallist = pcomms2list(msg)
    else:
        if not fields:
            fields = get_lcm_fields(msg)
        namelist = []
        vallist = []
        for f in fields:
            n, v = val2list(f,getattr(msg,f))
            namelist+=n
            vallist+=v
    return namelist, vallist

class Listener():
    def __init__(self,lc,logfile,unix_time,sample_time):
        # LCM object
        self.lc = lc
        # Logfile - already opened and ready to write
        self.logfile = logfile
        self.lcmtype = None
        self.msgfieds = None
        # Iso or unix time
        self.unix_time = unix_time
        # Sample time
        self.sample_time = sample_time
        self.t0 = time.time()
        # Counters
        self.cnt_logged = 0
        self.cnt_ignrd = 0

    def subscribe(self,channel):
        logging.info("Listening for LCM messages on channel <%s>"%channel)
        self.lc.subscribe(channel,self.handler)

    def handler(self,channel,data,tstamp=None):
        '''
        tstamp is an optional unix time stamp in seconds
        '''
        logging.debug("Rx <%s>"%channel)
        if tstamp == None:
            now = time.time()
        else:
            now = tstamp
        etime = now-self.t0
        if etime < 0:
            logging.warning("Elapsed time is negative <%f> - possible issue "
                            "if you see this more than once"%etime)
            etime = -1.0*etime
        if etime < self.sample_time:
            logging.debug("Decimating data based on specified sample time")
            self.cnt_ignrd+=1
        else:
            logging.debug("Writing data to file")
            self.cnt_logged+=1
            self.t0 = now
            # First time through, need to discover the lcm type
            writehead = False
            if not self.lcmtype:
                # Find the type
                self.lcmtype = find_lcm_type(data)
                if not self.lcmtype:
                    logging.fatal("Was not able to determine the LCM type of message!")
                    sys.exit()
                writehead = True

            msg = self.lcmtype.decode(data)
            # Get names of fields and values as two lists
            names,values = msg2list(msg)
            # Add timestamp
            if self.unix_time:
                theader = 'Unix Time [s]'
                if tstamp == None:
                    tstamp_str = "%.3f"%get_unix_time()
                else:
                    tstamp_str = "%.3f"%tstamp
            else:
                theader = 'Time [ISO 8601 formatted string]'  # Default
                if tstamp == None:
                    tstamp_str = datetime.datetime.now().isoformat()
                else:
                    tstamp_str = datetime.datetime.utcfromtimestamp(tstamp).isoformat()
            names.insert(0,theader)
            values.insert(0,tstamp_str)
            # Write to file
            if writehead:
                headstr = ",".join(names)
                self.logfile.write(headstr+'\n')
            valstr = ",".join(values)
            self.logfile.write(valstr+'\n') 

            logging.info("Wrote %d messages, ignored %d messages"
                         %(self.cnt_logged,self.cnt_ignrd))
            self.logfile.flush()

    def parselcmlog(self,fname,channel):
        logging.info("Attempting to open LCM log file <%s>"%fname)
        try:
            elog = lcm.EventLog(fname)
        except IOError:
            logging.fatal("The specified file <<"+fname+
                          ">> cannot be read by LCM!")
            sys.exit()
        event = elog.read_next_event()
        cnt=0
        while not (event==None):
            if event.channel == channel:
                self.handler(event.channel,event.data,
                             float(event.timestamp/1e6))
                cnt+=1
            event = elog.read_next_event()
        elog.close()
        msg = "Processed %d messages on channel <%s>"%(cnt,channel)
        if cnt > 0:
            logging.info(msg)
        else:
            logging.error(msg)
        logging.info("Wrote %d messages, ignored %d messages"%(self.cnt_logged,
                                                              self.cnt_ignrd))
        logging.info("That's all folks...")

def main(argv=None):
    # This enables us to invoke this function with arguments from a script
    if argv is None:
        argv = sys.argv

    # Argparse
    ex = "EXAMPLES: GO HERE"
    parser = argparse.ArgumentParser(formatter_class=
                                     argparse.ArgumentDefaultsHelpFormatter,
                                     epilog=ex,
                                     usage=__doc__)
    argparse_utils.add_args_lcm(parser)
    argparse_utils.add_args_verbosity(parser)
    parser.add_argument('-o','--output_dir',type=str,
                                 default=".",
                                 help="Output (logging) directory.")
    parser.add_argument('-f','--file_label',type=str,
                        help=("Output (log) filename label.  Generates a "
                              "file name with this label appended to the "
                              "date/time string.  If not specified, the "
                              "LCM channel will be used."))
    parser.add_argument('--unix_time',
                        help=("Use UNIX time float timestamps "
                              "instead of ISO 8601 formatted string"),
                        action="store_true")
    parser.add_argument('-s','--sample_time',type=float,
                        default=1.0,
                        help=('Minimium time between samples in seconds'))
    parser.add_argument('-i','--input_logfile',type=str,
                        default="",
                        help=('By default the program subscribes to existing LCM publications, ie. for real-time usage.  If you want to post-process existing LCM log files, give it a log file, it will instead parse the LCM log file.'))

    parser.add_argument("channel",help="Channel on which to subscribe")
    args = parser.parse_args()

    # Logging
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s ', datefmt='%Y%m%dT%H%M%SZ')
    logging.getLogger().setLevel(args.loglevel)
    logging.info("effective log level:    %s", logging.getLevelName(logging.getLogger().getEffectiveLevel()))
    logging.debug("Command line arguments: %s", args)

    # Open output CSV file
    if args.input_logfile:
        p,f = os.path.split(args.input_logfile)
        tstr=f
    else:
        tstr = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    label = "_"+args.channel+"_"
    if args.file_label:
        label += args.file_label+"_"
    fname =  (tstr + label + ".csv")
    fullfname = os.path.join(args.output_dir,fname)
    try:
        logfile = open(fullfname,'w')
        logging.info("Opened output CSV log file <%s>"%fullfname)
    except IOError:
        logging.fatal("Failed to open log file <%s>.  Check the path!"%fullfname)
        sys.exit()
    
    # LCM
    if args.lcm_url:
        lc = lcm.LCM(args.lcm_url)
    else:
        lc = lcm.LCM()
    
    # Initiate listener object
    listener = Listener(lc,logfile,args.unix_time,args.sample_time)
    
    if args.input_logfile:
        listener.parselcmlog(args.input_logfile,args.channel)
    else:
        listener.subscribe(args.channel)
        while True:
            logging.debug("Handling LCM...")
            try: 
                lc.handle()
            except KeyboardInterrupt:
                logging.info("Keyboard interruption")
    
if __name__ == "__main__":
    main()

        
