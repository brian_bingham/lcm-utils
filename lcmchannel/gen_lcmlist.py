
import sys
import inspect

lcmtypes = []
sys.path.append('/home/brian.bingham/MOE/wcd/fvs/trunk/python/lcmtypes')
import senlcm as t0
lcmtypes.append(t0)
sys.path.append('/home/brian.bingham/MOE/wcd/avs/trunk/python/lcmtypes')
import avslcm as t1
lcmtypes.append(t1)

def gen_lcmlist():
    lcmList =[]
    for ll in lcmtypes:
        for ii in inspect.getmembers(ll,inspect.isclass):
            lcmList.append(ii[1])
    return lcmList
