#!/usr/bin/env python

# lcmchannel inspired by rostopic

NAME='lcmchannel'

import os
import sys
import math
import socket
import time
import traceback
import select
import argparse

import lcm

# LCM types
#import oer
#import gss

        
def _lcmchannel_type(topic):
    """
    Print ROS message type of topic to screen
    :param topic: topic name, ``str``
    """
    import gen_lcmlist as genl
    lcmlist = genl.gen_lcmlist()
    print lcmlist

def _lcmchannel_echo(topic, callback_echo, bag_file=None, echo_all_topics=False):
    """
    Print new messages on topic to screen.
    
    :param topic: topic name, ``str``
    :param bag_file: name of bag file to echo messages from or ``None``, ``str``
    """
    
    pass

class ListCmd():
    def __init__(self):
        self.chlist = dict()
    def list_handler(self,channel,data):
        if channel in self.chlist:
            self.chlist[channel] += 1
        else:
            print(">> %s"%channel)
            self.chlist[channel] = 1


def _lcmchannel_cmd_list(argv):
    ex = "Here is an example"
    parser = argparse.ArgumentParser(prog="%s list"%NAME,
                                     formatter_class=
                                     argparse.ArgumentDefaultsHelpFormatter,
                                     epilog=ex)

    parser.add_argument("-t", "--time-window",
                        dest="twin", type=float, default=1.0,
                        help=("Time window to listen in seconds."),
                        metavar="TIME-WINDOW")

    parser.add_argument("-l", "--log-file",
                        dest="log", type=str, default='',
                        help=("LCM log file to read."),
                        metavar="LOG-FILE")
    argv = argv[2:]
    print argv
    args = parser.parse_args(argv)
    listcmd = ListCmd()
    lc = lcm.LCM()
    if args.log:
        print 'Lising topics from log file <%s>'%args.log
        if not(os.path.isfile(args.log)):
            print "The filename <%s> is not a valid file"%args.log
            sys.exit()
        try:
            elog = lcm.EventLog(args.log)
        except IOError:
            print "The specified file <<"+args.log+">> cannot be read by LCM"
            sys.exit()
        chDict = dict()
        event = elog.read_next_event()
        while event != None:
            if event.channel in chDict:
                chDict[event.channel] += 1
            else:
                chDict[event.channel] = 1
            event = elog.read_next_event()

        for k in chDict.keys():
            print('%s : %d'%(k,chDict[k]))
        

     

        
    else:
        #lc.subscribe("\w*",listcmd.list_handler)
        lc.subscribe(".*",listcmd.list_handler)
        print("Subscribing to all topics for %.2f seconds..."%args.twin)
        try:
            etime = 0.0
            last = time.time()
            while True:
                etime = time.time()-last
                timeout = args.twin - etime
                if timeout < 0.0:
                    break
                rfds, wfds, efds = select.select([lc.fileno()], [], [], timeout)
                if rfds:
                    lc.handle()
                else:
                    pass # timeout
                
        except KeyboardInterrupt:
            pass
        #listcmd.chlist.sort()
        if len(listcmd.chlist.keys()) > 0:
            print("\nSUMMARY:\nChannel \t Msgs[N] \t dT[ms] \t Rate[Hz]")
            for c in listcmd.chlist.keys():
                N = listcmd.chlist[c]
                dT = args.twin/float(N)
                print("%s \t %d \t\t %8.2f \t %8.2f"%(c,N,dT*1000,1/dT))
        else:
            print("No LCM publications!")

    


    
def _lcmchannel_cmd_echo(argv):
    pass

def _type_handler(channel,data):
    print "Received message on channl <%s> channel!"

def _lcmchannel_cmd_type(argv):
    '''
    Print LCM message type of channel to screen
    '''
    ex = "lcmchannel type rdi_dvl"
    parser = argparse.ArgumentParser(prog="%s type"%NAME,
                                     formatter_class=
                                     argparse.ArgumentDefaultsHelpFormatter,
                                     epilog=ex)
    
    parser.add_argument('channel',help="LCM channel",
                        metavar="CHANNEL")
    args = parser.parse_args(argv[2:])

    print "Subscribing to channel <%s>"%args.channel
    lc = lcm.LCM()
    lc.subscribe(args.channel,_type_handler)
    print "Listening..."
    lc.handle()
    '''
    import gen_lcmlist as genl
    lcmlist = genl.gen_lcmlist()
    print lcmlist
    '''
        
def _lcmchannel_cmd_pub(argv):
    """
    Parse 'pub' command arguments and run command. Will cause a system
    exit if command-line argument parsing fails.
    :param argv: command-line arguments
    :param argv: [str]
    :raises: :exc:`ROSTopicException` If call command cannot be executed
    """
    ex = "EXAMPLES: GO HERE"
    parser = argparse.ArgumentParser(prog="%s pub"%NAME,
                                     formatter_class=
                                     argparse.ArgumentDefaultsHelpFormatter,
                                     epilog=ex)
    parser.add_argument("channel",help="Channel on which to publish")
    parser.add_argument("lcm_type")
    parser.add_argument("-m","--message")
    parser.add_argument("-r", "--rate",
                        type=float, default=1.0,
                        help=("Publication rate in Hz."))
    parser.add_argument("-1", "--once",
                        action='store_true',
                        help=("Publish one message and exit."))
    args = parser.parse_args(argv)
    print args
    try:
        d = eval(args.message)
    except:
        print("can't eval message dictionary")
        d = {}
    print d

        
def _fullusage():
    print("""lcmtopic is a command-line tool for printing information about ROS Topics.

Commands:
\trostopic hz\tdisplay publishing rate of topic    
\trostopic list\tlist active topics
\trostopic pub\tpublish data to topic
\trostopic type\tprint topic type

Type rostopic <command> -h for more detailed usage, e.g. 'rostopic echo -h'
""")
    sys.exit(getattr(os, 'EX_USAGE', 1))

def lcmchannelmain(argv=None):
    if argv is None:
        argv=sys.argv    
    # process argv
    if len(argv) == 1:
        _fullusage()
    try:
        command = argv[1]
        if command == 'echo':
            _lcmchannel_cmd_echo(argv)
        elif command == 'type':
            _lcmchannel_cmd_type(argv)
        elif command == 'list':
            _lcmchannel_cmd_list(argv)
        elif command == 'info':
            _lcmchannel_cmd_info(argv)
        elif command == 'pub':
            _lcmchannel_cmd_pub(argv)
        else:
            _fullusage()
    except KeyboardInterrupt: pass

if __name__ == "__main__":
    lcmchannelmain()
