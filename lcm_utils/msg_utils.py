

import re

class LcmMsgUtilsParseException(Exception):
    pass

def split_args(args):
    """
    From Andy O.
    Parse the argument string into individual (signal,value) tuples and return
    a list of these tuples
    
    For example the arugment: 'signal1=value1,signal2=value2'
    would be returned as
    [('signal1','value1'), ('signal2','value2')]
    """
    if not args:
        return []
    
    individual_signals = re.split(',',args)
    sig_vals = []
    for s in individual_signals:
        sig_vals.append( re.split('=',s) )
        
    return sig_vals

def msg2str(msg):
    '''
    Converts lcm message to a readable string
    '''    
    return 's'

def str2msg(msg):
    '''
    Converts lcm message to a readable string
    '''    
    return 'm'
