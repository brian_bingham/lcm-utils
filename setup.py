from setuptools import setup

import sys
import re

from setuptools.command.test import test as TestCommand

# Use this idiom to check for any required modules that might not be installed
try:
    pass
    #import numpy
    #import scipy.interpolate
except ImportError:
    print ("Installing this software currently requires manual" 
           "installation of 'numpy' and 'scipy' packages, "
           "which appear to be missing")
    print "Install these packages and run setup again"
    sys.exit(-1)

# pull the version from the file
#version = re.search(
#    '^__version__\s*=\s*"(.*)"',
#    open('altimeter_valeport_lcm/altimeter_valeport_lcm.py').read(),
#    re.M
#    ).group(1)

# reuse the readme for the package description
with open("README.md", "rb") as f:
    long_descr = f.read().decode("utf-8")

# handle version based import requirements
install_requires = ['lcm'] #,'python-gss','python-oer','python-oer-utils']

name='lcm-utils'
setup(
    name=name,
    version=0,
    packages=['lcmchannel'],
    entry_points = {
        'console_scripts': 
        ['lcmchannel=lcmchannel.lcmchannel:lcmchannelmain']
        },
    url='https://bitbucket.org/brian_bingham/lcm-utils',
    license='TBD',
    author="Brian Bingham",
    long_description=long_descr,
    author_email='briansbingham@gmail.com',
    description='LCM Utilities',
    tests_require=['pytest'],
    install_requires=install_requires,
)
