#!/bin/bash
LOGDIR="/home/bsb/vault/lcm_streams"
# For testing, so I don't accumulate a bunch of log files
#rm ${LOGDIR}/*.csv

# For loggin nav
lcm2csvlog -v --output_dir=${LOGDIR} --sample_time=1.0 NAV_STAT

# For temp probe
#lcm2csvlog -v --output_dir=${LOGDIR} --sample_time=1.0 TEMP_PROBE_STAT

# Examples for post-processing log files
#LCMLOG=/home/bsb/WorkingCopies/lcm-utils/data/20170806_yogi_still.lcmlog 

#lcm2csvlog -v --input_logfile=${LCMLOG} --output_dir=${LOGDIR} --sample_time=1.0 NAV_STAT