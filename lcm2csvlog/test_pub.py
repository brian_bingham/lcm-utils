#!/usr/bin/env python
import lcm
import time
import oer
import gss

lc = lcm.LCM()

# For nav
channel = "NAV_STAT"
msg = gss.nav_solution_t()

# For temp probe
channel = 'TEMP_PROBE_STAT'
msg = gss.pcomms_t()
a = gss.analog_t()
a.name = 'temp'
a.value = 3.14
msg.analogs.append(a)
msg.num_analogs = 1
cnt = 0

while True:
    print "pub:%d, %s"%(cnt,channel)
    cnt+=1
    lc.publish(channel, msg.encode())
    time.sleep(0.1)
